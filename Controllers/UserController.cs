﻿using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using awang_dotnet_task.Model;
using Microsoft.Extensions.Configuration;

namespace awang_dotnet_task.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _connection;
        public UserController(IConfiguration connection)
        {
            _connection = connection;
        }

        //[HttpGet]
        //[Route("get_payment_id")]
        //public List<PaymentModel> GetPaymentId(int itemId)
        //{
        //    List<PaymentModel> OutputById = new List<PaymentModel>();
        //    OutputById = LoadPaymentDB().Where(x => x.payment_id == itemId).ToList();
        //    return OutputById;
        //}

        [HttpPost]
        [Route("AddUserWithTask")]
        public string AddUserWithTask(UserModel? inputs)
        {
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string queryInsertUser = "Insert into users values('" + inputs.name + "'); SELECT SCOPE_IDENTITY();";
                SqlCommand cmd = new SqlCommand(queryInsertUser, conn);

                var isertedID = cmd.ExecuteScalar();

                foreach (TaskModel tModel in inputs.tasks)
                {
                    string queryInsertTasks = "Insert into tasks values('" + tModel.task_detail + "'," + isertedID + ")";
                    cmd = new SqlCommand(queryInsertTasks, conn);
                    cmd.ExecuteNonQuery();
                }
                conn.Close();

            }
            return "Item Added Successfully";

        }


        [HttpGet]
        [Route("api/tasks/GetUserWithTask")]
        public List<UserModel> GetUsers(string? name)
        {

            List<UserModel> users = new List<UserModel>();

            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                string queryUsers = "select * from Users";
                if (!String.IsNullOrEmpty(name))
                {
                    queryUsers = $"select * from Users where name like '%{name}%'";
                }
                SqlCommand cmdUsers = new SqlCommand(queryUsers, connection);

                SqlDataAdapter adapterUsers = new SqlDataAdapter(cmdUsers);
                DataTable dtUsers = new DataTable();
                adapterUsers.Fill(dtUsers);

                for (int i = 0; i < dtUsers.Rows.Count; i++)
                {
                    UserModel userModel = new UserModel();

                    int user_id = Convert.ToInt32(dtUsers.Rows[i]["pk_users_id"].ToString());

                    userModel.pk_users_id = user_id;
                    userModel.name = dtUsers.Rows[i]["name"].ToString();

                    string getTasksUser = $"select * from Tasks where fk_users_id = {user_id}";
                    SqlCommand cmdTaskUser = new SqlCommand(getTasksUser, connection);
                    SqlDataAdapter adapterTaskUser = new SqlDataAdapter(cmdTaskUser);
                    DataTable dtTaskUsers = new DataTable();
                    adapterTaskUser.Fill(dtTaskUsers);

                    List<TaskModel> listTaskUser = new List<TaskModel>();

                    for (int j = 0; j < dtTaskUsers.Rows.Count; j++)
                    {
                        TaskModel taskModel = new TaskModel();
                        taskModel.pk_tasks_id = Convert.ToInt32(dtTaskUsers.Rows[j]["pk_tasks_id"].ToString());
                        taskModel.task_detail = dtTaskUsers.Rows[j]["task_detail"].ToString();
                        listTaskUser.Add(taskModel);
                    }
                    userModel.tasks = listTaskUser;
                    users.Add(userModel);
                }

                connection.Close();

                return users;
            }


        }
    }
}
