﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using awang_dotnet_task.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace awang_dotnet_task.Controllers
{

    //Route["api/[Controllers"]
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public TokenController(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        [HttpGet]
        [Route("GenerateToken")]
        public ActionResult GenerateToken()
        {
            string JwtToken = "";
            //


            return Ok(JwtToken);
        }


        [HttpGet]
        [Route("ValidateToken")]
        [Authorize]
        public ActionResult ValidateToken()
        {
            return Ok("Congrats!, your token is right");
        }

        private UserModelAuth Authenticate(UserLogin userLogin)
        {
            var currentUser = UserConstant.Users.FirstOrDefault(o => o.name.ToLower() == userLogin.name.ToLower());

            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }

        private string Generate(UserModelAuth user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:issuer"],
                _configuration["Jwt:Audience"],
                null,
                expires: DateTime.Now.AddMinutes(3),
                signingCredentials: credentials
                );

            return new JwtSecurityTokenHandler().WriteToken(token);

        }
    }
}
